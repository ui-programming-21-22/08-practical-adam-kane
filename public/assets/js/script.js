//window.unload= function(){}

let adiv = document.getElementById('time');

function time()
{
    let date = new Date();

    adiv.innerHTML = (+date.getHours()
    +":"+date.getMinutes()
    +":"+date.getSeconds()
    +"<br>" 
    + date.getDate()
    +"/"+(date.getMonth()+1)
    +"/"+date.getFullYear());
}

const canvas = document.getElementById("d_canvas")
const context = canvas.getContext("2d");

const scale = 5;
const width = 17;
const height = 17;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 1, 0, 0];
const frameLimit = 16;


let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 1.5;

//assigning player image
let playerImage = new Image();
playerImage.src = "assets/media/black-mage-overworld-sprite.png";

function drawFrame(playerImage, frameX, frameY, canvasX, canvasY) {
    context.drawImage(playerImage,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

// GameObject holds positional information
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    //this.mvmtDirection = "None";
}

// Default Player
let player = new GameObject(playerImage, 0, 0, 200, 200);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

// Take Input from the Player
function input(event) {
    
    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break;
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            case 49: //Number 1
                gamerInput = new GamerInput("1");
            break; 
            case 50: //Number 2 
                gamerInput = new GamerInput("2");
            break;
            case 51: //Number 3
                gamerInput = new GamerInput("3");
            break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}


function changeCharacter()
{
    if (gamerInput.action === "1")
        {
            playerImage.src = "assets/media/black-mage-overworld-sprite.png";

        }
    if (gamerInput.action === "2")
        {
            playerImage.src = "assets/media/fighter-overworld-sprite.png";
        }
    if (gamerInput.action === "3")
        {
            playerImage.src = "assets/media/white-mage-overworld-sprites.png";
        }
}

function update() {

    time();
    changeCharacter();

    // Move Player Up
    if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.y -= speed;
        currentDirection = 2;
    }
    // Move Player Down
    else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.y += speed;
        currentDirection = 0; 
    }
    // Move Player Left 
    else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.x -= speed;
        currentDirection = 1; 
    }
    // Move Player Right
    else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.x += speed;
        currentDirection = 3; 
    }

}

function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}


function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    //console.log(player);
    animate();
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);

// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);